const express = require("express");
const app = express();
const {
  sendMessage,
  deleteSession,
  checkActiveWhatsApp,
} = require("../module-wabot/index");

app.post("/api/send-chat", async (req, res, next) => {
  let source = req.body;
  let sessionId = req.headers.token;

  if (!sessionId) {
    sessionId = source.token;
  }
  if (!source.to || !source.message || !sessionId) {
    return res.json({ status: 500, message: "Missing payload data" });
  } else {
    const payload = {
      message: source.message,
      image: source.image ? source.image : null,
    };
    const result = await sendMessage(sessionId, source.to, payload, null);
    console.log("result : ", result);
    if (result !== "Message Send!") {
      return res.json({
        status: 500,
        message: result,
      });
    }

    return res.json({
      status: 200,
      data: {
        msg: {
          to: source.to,
          body: {
            text: source.message,
          },
        },
      },
    });
  }
});

app.post("/api/remove-instance", async (req, res, next) => {
  let source = req.body;
  let sessionId = req.headers.token;
  if (!sessionId) {
    sessionId = source.token;
  }
  try {
    if (!sessionId)
      return res.json({ status: 400, message: "Missing payload token" });
    const removeInstance = await deleteSession(sessionId, null);
    return res.json({
      status: 200,
      message: removeInstance,
    });
  } catch (error) {
    return res.json({ status: 500, message: error });
  }
});

app.post("/api/check-whatsapp", async (req, res, next) => {
  let source = req.body;
  let sessionId = req.headers.token;
  if (!sessionId) {
    sessionId = source.token;
  }

  if (!source.phone || !sessionId) {
    return res.json({ status: 500, message: "Missing payload data" });
  }

  try {
    const result = await checkActiveWhatsApp(sessionId, source.phone);
    return res.json(result);
  } catch (error) {
    return res.json({ status: 500, message: error });
  }
});

module.exports = app;
