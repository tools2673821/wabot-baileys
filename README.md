# Payuni Addon Wa Bot


# Installation and Configuration

### Configuration

- setting up database configuration in /config/db.json
- setting up license configuration in /config/license.json

### Running using docker-compose

- please open index.js
- if you dont want to validate the machineId for your production, comment the request axios on the index.js

```bash
$ git clone https://gitlab.com/tools2673821/wabot-baileys
$ cd wabot-baileys
$ docker-compose up -d --build
```

## Credits
Built with Nodejs v16

## Docs websocket
- The action options : 
  * add-device (add new device for multiple connection)
  * reconnect (delete existing session and create new session)
  * end-session (delete session)
  * send-chat (send message by sessionId / room)

### payload request
```bash
  {
    "room": "123asd123",
    "message": {
        "action": "send-chat",
        "msg": {
          "message": "Hello World",
          "image" : "base64"
        }
    }
  }
```

### API Send Chant

- url : http://localhost:3000/api/send-chat
- merhod: POST

#### headers
```bash
  headers: {
    token: $token
  }
```
#### payload 
```bash
  {
      "to": "085271662516",
      "message": "Hello Dear",
      "image": "base64"
  }
```


### API Remove Instance

- url : http://localhost:3000/api/remove-instance
- merhod: POST

#### headers
```bash
  headers: {
    token: $token
  }
```

### Docs Build PKG

```bash
$ npm install
$ npm run build
```
After build is completed, ensure you provide directory /config (db.json & license.json) and .env file parallel with pkg build result

structure :

  - file-build-(linux/macos/.exe)
  - /config
      - db.json
      - license.json
  - .env

---

Copyright ©️ 2023 by PT. Fintek Digital Nusantara