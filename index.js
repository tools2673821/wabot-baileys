const {
  create,
  reconnect,
  sendMessage,
  deleteSession,
} = require("./module-wabot/index");
const mongoose = require("mongoose");
const cors = require("cors");
const axios = require("axios");
const express = require("express");
const socketIo = require("socket.io");
const app = express();
const server = require("http").Server(app);
const apiWa = require("./api/wapi");
const machineId = require("node-machine-id");
const ClientInstance = require("./models/devices_model");
console.log("machine ID :", machineId.machineIdSync());
const fs = require("fs");

const path = require("path");
const configFolderPath = process.cwd();
const envConfigPath = path.join(configFolderPath, ".env");
require("dotenv").config({ path: envConfigPath });
const envData = fs.readFileSync(envConfigPath, "utf-8");
const envLines = envData.split("\n");
let port;
let mode;
envLines.forEach((line) => {
  if (line.includes("NODE_PORT")) {
    const [key, value] = line.split("=");
    port = value;
  }
  if (line.includes("NODE_ENV")) {
    const [key, value] = line.split("=");
    mode = value;
  }
});
const configDBPath = path.join(configFolderPath, "config", "db.json");
const configDB = JSON.parse(fs.readFileSync(configDBPath, "utf8"))[mode];
const configLicensePath = path.join(configFolderPath, "config", "license.json");
const configLicense = JSON.parse(fs.readFileSync(configLicensePath, "utf-8"))[
  mode
];
const io = socketIo(server, {
  cors: {
    origin: "*",
    credentials: true,
  },
});
global.baileys = {};

app.use(cors());
app.use(express.json({ limit: "50mb" }));
app.use("/", apiWa);

let authMongo = configDB.password
  ? `${configDB.username}:${configDB.password}@`
  : "";

// axios
//   .get(
//     `${configLicense.host}/validate?key=${machineId.machineIdSync()}&type=${
//       configLicense.type
//     }`
//   )
//   .then((response) => {
mongoose
  .connect(
    `mongodb://${authMongo}${configDB.host}:${configDB.port}/${configDB.db}`,
    {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    }
  )
  .then(async () => {
    console.log(`[!] Mongodb connected ${configDB.host}`);
    // Reconnect instance
    let clientInstance = await ClientInstance.find({
      status: 1,
    }).select("_id");

    if (clientInstance.length) {
      clientInstance = JSON.parse(JSON.stringify(clientInstance));
      for (const instance of clientInstance) {
        console.log(`Process Reconnect For SessionId[${instance._id}]`);
        await create(instance._id);
      }
    }

    io.on("connection", (socket) => {
      console.log("New Client connected");

      socket.on("join", (room) => {
        socket.join(room);
      });

      socket.on("message", ({ room, message }) => {
        console.log("message", message);
        const { action, direction, msg } = message;
        if (action == "add-device") {
          create(room, socket);
        } else if (action == "reconnect") {
          reconnect(room, socket);
        } else if (action == "end-session") {
          deleteSession(room, socket);
        } else if (action == "send-chat") {
          sendMessage(room, direction, msg, socket);
        }
      });

      socket.on("disconnect", () => {
        console.log("Client disconnected");
      });
    });
    server.listen(port, () => console.log(`Listening on port ${port}`));
  })
  .catch((error) => console.log(`[!] Error Mongodb : ${error}`));
// })
// .catch((error) => {
//   console.log("[!] Error : ", error.response?.data?.message ?? error.message);
// });

/**
 * - DOCS
 * add-device (add new device for multiple connection)
 * reconnect (delete existing session and create new session)
 * end-session (delete session)
 * send-chat (send message by sessionId / room)
 * 
 * payload request
 *{
    "room": "123asd123",
    "message": {
        "action": "add-device",
        "direction": "6285127182912",
        "msg": {
          "message": "hello world",
          "image" : "base64img"
        }
    }
  }
 */
