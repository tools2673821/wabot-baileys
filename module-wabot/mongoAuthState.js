const { proto } = require("@whiskeysockets/baileys/WAProto");
const {
  generateRegistrationId,
} = require("@whiskeysockets/baileys/lib/Utils/generics");
const {
  BufferJSON,
  initAuthCreds,
} = require("@whiskeysockets/baileys/lib/Utils");

module.exports = useMongoDBAuthState = async (collection, sessionId) => {
  const writeData = (data, id) => {
    const informationToStore = JSON.parse(
      JSON.stringify(data, BufferJSON.replacer)
    );
    const update = {
      $set: {
        ...informationToStore,
      },
    };
    return collection.updateOne({ _id: id }, update, { upsert: true });
  };
  const readData = async (id) => {
    try {
      const data = JSON.stringify(await collection.findOne({ _id: id }));
      return JSON.parse(data, BufferJSON.reviver);
    } catch (error) {
      return null;
    }
  };
  const removeData = async (id) => {
    try {
      await collection.deleteOne({ _id: id });
    } catch (_a) {}
  };
  const creds = (await readData(sessionId)) || (0, initAuthCreds)();
  return {
    state: {
      creds,
      keys: {
        get: async (type, ids) => {
          const data = {};
          await Promise.all(
            ids.map(async (id) => {
              let value = await readData(`${sessionId}-${type}-${id}`);
              if (type === `${sessionId}-app-state-sync-key`) {
                value = proto.Message.AppStateSyncKeyData.fromObject(data);
              }
              data[id] = value;
            })
          );
          return data;
        },
        set: async (data) => {
          const tasks = [];
          for (const category of Object.keys(data)) {
            for (const id of Object.keys(data[category])) {
              const value = data[category][id];
              const key = `${sessionId}-${category}-${id}`;
              tasks.push(value ? writeData(value, key) : removeData(key));
            }
          }
          await Promise.all(tasks);
        },
      },
    },
    saveCreds: () => {
      return writeData(creds, sessionId);
    },
  };
};
