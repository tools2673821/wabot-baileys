const WhatsAppBot = require("./whatsappClient.js");

const { MongoClient } = require("mongodb");

const fs = require("fs");

const path = require("path");
const configFolderPath = process.cwd();
const envConfigPath = path.join(configFolderPath, ".env");
require("dotenv").config({ path: envConfigPath });
const envData = fs.readFileSync(envConfigPath, "utf-8");
const envLines = envData.split("\n");
let mode;
envLines.forEach((line) => {
  if (line.includes("NODE_ENV")) {
    const [key, value] = line.split("=");
    mode = value;
  }
});
const configDBPath = path.join(configFolderPath, "config", "db.json");
const configDB = JSON.parse(fs.readFileSync(configDBPath, "utf8"))[mode];

const ClientInstance = require("../models/devices_model");

async function getCollection() {
  const authMongo = configDB.password
    ? `${configDB.username}:${configDB.password}@`
    : "";
  const mongoURL = `mongodb://${authMongo}${configDB.host}:${configDB.port}`;
  const mongoClient = new MongoClient(mongoURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  await mongoClient.connect();
  return mongoClient.db(configDB.db).collection("auth_info_baileys");
}

async function create(sessionId, socket) {
  if (baileys[sessionId]) {
    let msg = `SessionId[${sessionId}] is exist`;
    console.log(msg);
    if (socket) emitSocket(socket, sessionId, 409, msg);
    return msg;
  }

  await WhatsAppBot(sessionId, await getCollection(), socket);
}

async function sendMessage(sessionId, direction, messages, socket) {
  let msg;
  if (!baileys[sessionId]) {
    msg = `SessionId[${sessionId}] not found`;
    console.log(msg);
    return msg;
  }

  try {
    const to = direction.replace(new RegExp("^08"), "628") + "@s.whatsapp.net";
    const { message, image } = messages;
    let payloadMsg = {};
    if (image) {
      payloadMsg = {
        image: Buffer.from(image.split(",")[1], "base64"),
        caption: message,
      };
    } else {
      payloadMsg = {
        text: message,
      };
    }

    await baileys[sessionId].sendMessage(to, payloadMsg);
    console.log(`WABOT[${sessionId}] send message to : [${direction}]`);
    msg = "Message Send!";
    console.log(msg);
    if (socket) emitSocket(socket, sessionId, 200, msg);
    return msg;
  } catch (error) {
    console.error(error);
    if (socket) emitSocket(socket, sessionId, 500, error);
    return error;
  }
}

async function deleteSession(sessionId, socket) {
  try {
    if (baileys[sessionId]) {
      await baileys[sessionId].logout();
      delete baileys[sessionId];
    }
    const collection = await getCollection();
    await collection.deleteMany({
      _id: { $regex: new RegExp(`^${sessionId}`) },
    });
    let msg = `SessionId[${sessionId}] success delete`;
    console.log(msg);
    if (socket) emitSocket(socket, sessionId, 200, msg);
    await ClientInstance.findOneAndUpdate({ _id: sessionId }, { status: 0 });
    return msg;
  } catch (error) {
    console.error(error);
    if (socket) emitSocket(socket, sessionId, 500, error);
    return error;
  }
}

async function reconnect(sessionId, socket) {
  try {
    if (baileys[sessionId]) {
      await baileys[sessionId].logout();
      delete baileys[sessionId];
    }
    const collection = await getCollection();
    await collection.deleteMany({
      _id: { $regex: new RegExp(`^${sessionId}`) },
    });
    await create(sessionId, socket);
  } catch (error) {
    console.error(error);
    if (socket) emitSocket(socket, sessionId, 500, error);
    return error;
  }
}

async function checkActiveWhatsApp(sessionId, phone) {
  try {
    if (!baileys[sessionId]) {
      return {
        status: 404,
        message: `SessionId[${sessionId}] not found`,
      };
    }

    const [result] = await baileys[sessionId].onWhatsApp(
      phone.replace(new RegExp("^08"), "628")
    );
    if (result && result.exists) {
      return {
        status: 200,
        valid: true,
        data: { id: phone },
      };
    } else {
      return {
        status: 404,
        valid: false,
        data: { id: phone },
      };
    }
  } catch (error) {
    console.log("disini error :", error);
    return {
      status: 500,
      valid: false,
      data: { id: phone },
      message: error,
    };
  }
}

/**
 * Emit Webcosket
 */
function emitSocket(socket, room, status, message) {
  return socket.emit("message", {
    room: room,
    message: {
      status,
      data: message,
    },
  });
}

module.exports = {
  create,
  sendMessage,
  deleteSession,
  reconnect,
  checkActiveWhatsApp,
};
