const {
  DisconnectReason,
  useMultiFileAuthState,
} = require("@whiskeysockets/baileys");
const useMongoDBAuthState = require("./mongoAuthState");
const makeWASocket = require("@whiskeysockets/baileys").default;
const axios = require("axios");
const ClientInstance = require("../models/devices_model");
const QRCode = require("qrcode");

async function connectionLogic(sessionId, collection, socket) {
  let qrCount = 0;
  const MAX_QR_COUNT = 5;
  const { state, saveCreds } = await useMongoDBAuthState(collection, sessionId);
  baileys[sessionId] = makeWASocket({
    printQRInTerminal: true,
    auth: state,
    browser: ["Findig"]
  });

  baileys[sessionId].ev.on("connection.update", async (update) => {
    const { connection, lastDisconnect, qr, receivedPendingNotifications } =
      update || {};

    if (qr) {
      qrCount++;

      if (qrCount > MAX_QR_COUNT) {
        try {
          console.log(
            `SessionId[${sessionId}] mencapai batas maksimum qrcode. Proses dihentikan`
          );
          await baileys[sessionId].logout();
          delete baileys[sessionId];
          if (socket) emitSocket(socket, sessionId, 500, "Not Logged");
          qrCount = 0;
          await ClientInstance.findOneAndUpdate(
            { _id: sessionId },
            { status: 0 }
          );
        } catch (error) {
          console.log(`error on sessionId[${sessionId}]: ${error}`);
        }
        return;
      }

      console.log(`String QRCode for sessionId[${sessionId}] : `, qr);
      if (socket)
        emitSocket(socket, sessionId, 201, await QRCode.toDataURL(qr));
    }

    if (connection === "close") {
      const shouldReconnect =
        lastDisconnect?.error?.output?.statusCode !==
        DisconnectReason.loggedOut;

      const blockedAccount = lastDisconnect?.error?.output?.statusCode == 403    

      if(blockedAccount){
        console.log(`\nSessionId[${sessionId}]: HAS BEEN BLOCKED\n`);
        delete baileys[sessionId];
        try {
          await ClientInstance.findOneAndUpdate(
            { _id: sessionId },
            { status: 0 }
          );
        } catch (error) {
          console.log(`error update status to 0 sessionId[${sessionId}]: ${error}`);
        }
        return;
      }
      
      // reconnect if not logged out
      if (shouldReconnect) {
        console.log(`SessionId[${sessionId}]: shoud reconnect`);
        await connectionLogic(sessionId, collection, socket);
      } else {
        //delete session if logout by phone devices
        console.log(`\nSessionId[${sessionId}]: HAS BEEN LOGOUT BY DEVICES\n`);
        delete baileys[sessionId];
        try {
          await ClientInstance.findOneAndUpdate(
            { _id: sessionId },
            { status: 0 }
          );
        } catch (error) {
          console.log(`error update status to 0 sessionId[${sessionId}]: ${error}`);
        }
      }
    }

    if (receivedPendingNotifications) {
      let msg = `Session [${sessionId}] is connected`;
      console.log(msg);
      try {
        await ClientInstance.findOneAndUpdate(
          { _id: sessionId },
          { status: 1 }
        );
      } catch (error) {
        console.log(`error on sessionId[${sessionId}]: ${error}`);
      }
      if (socket) emitSocket(socket, sessionId, 200, "Login Success");
    }
  });

  baileys[sessionId].ev.on("messages.upsert", async (m) => {
    //listener personal chat
    if (
      m.messages[0].key.fromMe === false &&
      m.messages[0].key.participant == undefined
    ) {
      let recipient = m.messages[0]?.key?.remoteJid;
      recipient = recipient.replace("@s.whatsapp.net", "");
      let message = m.messages[0]?.message;
      message =
        message?.conversation || message?.extendedTextMessage?.text || "";
      if (message && message != "") {
        try {
          console.log(`SessionId[${sessionId}] get message :`, message);
          //reading message
          await baileys[sessionId].readMessages([m.messages[0]?.key]);
          const data = await ClientInstance.findOne({ _id: sessionId });
          if (data) {
            axios
              .post(
                data.webhook,
                {
                  from: data.phone,
                  to: recipient,
                  message: message,
                  data: m,
                },
                {
                  headers: {
                    token: data._doc.token,
                  },
                }
              )
              .then((response) => console.log("response api : ", response.data))
              .catch((error) => console.log("error api : ", error));
          }
        } catch (error) {
          console.log(`error on sessionId[${sessionId}]: ${error} `);
        }
      } else {
        console.log(
          `SessionId[${sessionId}] can't listen message :`,
          m.messages[0]
        );
      }
    }

    // listener chat group
    if (
      m.messages[0].key.fromMe === false &&
      m.messages[0].key.participant != undefined
    ) {
      let recipient = m.messages[0]?.key?.participant;
      recipient = recipient.replace("@s.whatsapp.net", "");
      let message = m.messages[0]?.message;
      message = message?.conversation || message?.extendedTextMessage?.text;
      try {
        const data = await ClientInstance.findOne({ _id: sessionId });
        if (data) {
          let ownNumber = data.phone;
          ownNumber = "@" + ownNumber.replace(new RegExp("^08"), "628");

          //jika di mention di grop
          if (message?.includes(ownNumber)) {
            console.log(
              `SessionId[${sessionId}] get message from group :`,
              message
            );

            //reading message
            await baileys[sessionId].readMessages([m.messages[0]?.key]);

            axios
              .post(
                data.webhook,
                {
                  from: data.phone,
                  to: recipient,
                  message_group: message?.replace(ownNumber, ""),
                  data: m,
                },
                {
                  headers: {
                    token: data._doc.token,
                  },
                }
              )
              .then((response) => console.log("response api : ", response.data))
              .catch((error) => console.log("error api : ", error));
          }
        }
      } catch (error) {
        console.log(`error on sessionId[${sessionId}]: ${error} `);
      }
    }
  });

  baileys[sessionId].ev.on("creds.update", saveCreds);
}

/**
 * Emit Webcosket
 */
function emitSocket(socket, room, status, message) {
  return socket.emit("message", {
    room: room,
    message: {
      status,
      data: message,
    },
  });
}

module.exports = connectionLogic;
