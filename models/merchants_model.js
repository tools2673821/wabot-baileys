const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const merchantsModel = new Schema({
  nama_merchant: {
    type: String,
    required: true,
  },
  aktif: {
    type: Boolean,
    required: true,
  },
  email_merchant: {
    type: String,
    required: true,
    unique: true,
  },
  logo_merchant: {
    type: String,
  },
});

module.exports = exports = mongoose.model(
  "merchantsModel",
  merchantsModel,
  "merchants"
);
