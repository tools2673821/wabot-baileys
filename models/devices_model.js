const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const devicesModel = new Schema({
  instanceName: {
    type: String,
  },
  status: {
    type: Number,
    default: 0,
  },
  webhook: {
    type: String,
  },
  phone: {
    type: String,
  },
  token: {
    type: String,
  },
  kode_merchant: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: "merchantsModel",
  },
});

module.exports = exports = mongoose.model(
  "devicesModel",
  devicesModel,
  "devices"
);
